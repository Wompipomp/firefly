﻿using UnityEngine;
using System.Collections;

public class SelfDestroy : MonoBehaviour {
	public float DestroyInSeconds = 2;

	// Use this for initialization
	void Start () {
		Invoke("SelfDestruct", DestroyInSeconds);
	}

	void SelfDestruct()
	{
		Destroy(gameObject);
	}
	
	
}
