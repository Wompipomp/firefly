﻿using UnityEngine;
using System.Collections;

public class MoveLeft : MonoBehaviour {

    public float MoveSpeed = 2;

    private Transform cachedTransform;
    private Vector3 moveVector;
	// Use this for initialization
	void Start () {
        cachedTransform = transform;
        moveVector = new Vector3(-MoveSpeed, 0, 0);
        moveVector = transform.TransformDirection(moveVector);
    }
	
	// Update is called once per frame
	void Update () {
        cachedTransform.Translate(moveVector * Time.deltaTime, Space.World);
    }
}
