﻿using UnityEngine;
using System.Collections;

public static class Extensions {
    private const int maxAttenuation = 80;

    public static float orthographicHorizontalSize(this Camera cam)
    {
        var verticalSize = cam.orthographicSize;
        return verticalSize * cam.aspect;
    }

    public static bool MatchLayer(this LayerMask thisMask, LayerMask otherMask)
    {
        return (thisMask & 1 << otherMask) != 0;
    }

    public static float VolumeToAttenuation(this float volume)
    {
        return -((1-volume) * maxAttenuation);
    }

 }
