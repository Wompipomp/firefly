﻿using UnityEngine;
using System.Collections;

public class Settings {
    public float GetMusicVolume()
    {
        return PlayerPrefs.GetFloat(PlayerPrefsConstants.MusicVolume, 0.8f);
    }

    public void SetMusicVolume(float value)
    {
        
        PlayerPrefs.SetFloat(PlayerPrefsConstants.MusicVolume, value);
    }

    public float GetFxVolume()
    {
        return PlayerPrefs.GetFloat(PlayerPrefsConstants.FxVolume, 1);
    }

    public void SetFxVolume(float value)
    {
        PlayerPrefs.SetFloat(PlayerPrefsConstants.FxVolume, value);
    }
}
