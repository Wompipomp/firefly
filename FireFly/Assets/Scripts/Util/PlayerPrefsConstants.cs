﻿using UnityEngine;
using System.Collections;

public class PlayerPrefsConstants {
    public const string MusicVolume = "MusicVolume";
    public const string FxVolume = "FxVolume";
}
