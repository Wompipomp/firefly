﻿using UnityEngine;
using System.Collections;

public class Tags {

    public const string Enemy = "Enemy";
    public const string Player = "Player";
    public const string Shot = "Shot";
    public const string Boundary = "Boundary";
}
