﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour {

    public float Rotation;
    private Rigidbody2D rb2D;
    // Use this for initialization
	void Awake () {
        rb2D = GetComponent<Rigidbody2D>();
	}

    void Start()
    {
        rb2D.angularVelocity = Rotation;
    }
	

}
