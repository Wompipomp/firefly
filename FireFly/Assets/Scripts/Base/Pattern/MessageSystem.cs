﻿using UnityEngine.Events;
using System.Collections.Generic;

public class MessageSystem : PersistentSingleton<MessageSystem> {
    public const string StartPauseEvent = "StartPauseEvent";
    public const string EndPauseEvent = "EndPauseEvent";

    private Dictionary<string, UnityEvent> events = new Dictionary<string, UnityEvent>();

    public void AddListener(string eventId, UnityAction listener)
    {
        UnityEvent thisEvent = null;
        if(events.TryGetValue(eventId, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new UnityEvent();
            thisEvent.AddListener(listener);
            events.Add(eventId, thisEvent);
        }
        
    }

    public void RemoveListener(string eventId, UnityAction listener)
    {
        if (events == null)
            return;
        UnityEvent thisEvent = null;
        if(events.TryGetValue(eventId, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public void Invoke(string eventId)
    {
        UnityEvent thisEvent = null;
        if(events.TryGetValue(eventId, out thisEvent))
        {
            thisEvent.Invoke();
        }
    }

   

}
