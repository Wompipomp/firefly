﻿using UnityEngine;
using System.Collections;

public abstract class BaseEnemyController : MonoBehaviour {
    protected Transform cachedTransform;
    public int Health = 100;
    public int Points;
    public float FirstShotDelayMin;
    public float FirstShotDelayMax;
    public float RepeatShotDelay;
    private WeaponController weaponController;
    private Transform player;

    void Awake()
    {
        cachedTransform = transform;
        weaponController = GetComponent<WeaponController>();
        player = GameManager.Instance.Player.transform;
    }

    public void Start()
    {
       
            InvokeRepeating("TryFireWeapon", Random.Range(FirstShotDelayMin,FirstShotDelayMax), RepeatShotDelay);
    }

    private void TryFireWeapon()
    {
        if (transform.position.x > player.position.x) // Only shoot when in front of player
            weaponController.FireProjectile();
    }

    public abstract void Init(InitEnemyDto dto);

    public abstract void Update();
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag(Tags.Player))
        {
            var playerController = other.gameObject.GetComponent<PlayerController>();
            playerController.AddDamage(70);
            CancelInvoke();
            DestroyShip();
        }
        if(other.gameObject.CompareTag(Tags.Boundary))
        {
            CancelInvoke();
            Destroy(gameObject);
        }
       
    }

    private void DestroyShip()
    {
        SoundController.Instance.PlayRandomEnemyExplosion();
        SpawnController.Instance.SpawnExplosion(ExplosionsIds.ExplosionBig, gameObject.transform.position, false);
        Destroy(gameObject);
    }

    public void AddDamage(int damage)
    {
        var rightCameraBound = Camera.main.orthographicHorizontalSize();
        if (rightCameraBound < transform.position.x) //If not visible no damage
            return;
        Health -= damage;
        if(Health <= 0)
        {
            GameManager.Instance.AddPoints(Points);
            DestroyShip();

        }
    }
    
}

public enum EnemyType
{
    Simple,
    Waypoint
}
