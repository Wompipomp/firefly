﻿using UnityEngine;
using System.Collections;

public class EnemyMoveController : BaseEnemyController {
    private Vector3 moveVector;


    public override void Init(InitEnemyDto dto)
    {
        moveVector = new Vector3(-dto.Speed, 0, 0);
        moveVector = transform.TransformDirection(moveVector);
    }

    public override void Update()
    {
        cachedTransform.Translate(moveVector * Time.deltaTime, Space.World);
    }
}
