﻿using UnityEngine;
using System.Collections;

public class EnemyWeaponContoller : WeaponController { //
    private Vector3 dir;
    private Transform player;

    void Awake()
    {
        player = GameManager.Instance.Player.transform;
    }
	// Update is called once per frame
	void Update () {
        dir = player.position - WeaponSlotPosition.transform.position;
        WeaponSlotPosition.rotation = Quaternion.FromToRotation(Vector3.right, dir);
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawRay(WeaponSlotPosition.transform.position, dir.normalized);
    }
}
