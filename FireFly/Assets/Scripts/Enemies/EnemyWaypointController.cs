﻿using UnityEngine;
using System.Collections;

public class EnemyWaypointController : BaseEnemyController {
    private WaypointList waypointList;
    private float startTime;
    private float Duration = 5;
    private float progress;

   
    public override void Init(InitEnemyDto dto)
    {
        Duration = dto.Speed;
        waypointList = dto.Waypoints;
        transform.position = waypointList.Waypoints[0];
        startTime = Time.time;

    }

    public override void Update()
    {
        progress = (Time.time - startTime) / Duration;
        if (progress < 1)
            transform.position = waypointList.GetPoint(progress);
        else
            Destroy(gameObject);
    }
}
