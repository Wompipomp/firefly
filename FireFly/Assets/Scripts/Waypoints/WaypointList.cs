﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class WaypointList : MonoBehaviour {
    public List<Vector2> Waypoints;
    public int LineSteps = 10;

    public Vector2 GetPoint(float t)
    {
        //Quelle: Catlike Tutorial: http://catlikecoding.com/unity/tutorials/curves-and-splines/
        t = Mathf.Clamp01(t) * CurveCount; //0-1 auf Anzahl der Curven stretchen => Somit ist z.B. t = 0.5 bei 2 Curven dann die 2te Kurve
        var i = (int)t; //Abrunden da die es bei den Punkten keine float gibt
        t -= i; //t zwischen den Curven berechnen => z.B. bei 2 Kurven und 0.7 = 2 * 0.7 => 1.4 => Kurve 2 ist an der REihe und t in dieser Kurve ist dann 0.4 => 1.4 - 1 <=> t - i
        i *= 2; //Multipliziert mit der Anzahl der Waypoints die jede Kurve braucht
            
        return GetPoint(Waypoints[i], Waypoints[i+1], Waypoints[i+2], t);
    }
    public Vector2 GetPoint(Vector2 p0, Vector2 p1, Vector2 p2, float t)
    {
        t = Mathf.Clamp01(t);
        var oneMinusT = 1f - t;
        return oneMinusT * oneMinusT * p0 + 2f * oneMinusT * t * p1 + t * t * p2;
    }

    private int CurveCount
    {
        get
        {
            return (int)Waypoints.Count / 2;
        }
    }

    public void Reset()
    {
        Waypoints = new List<Vector2>();
        Waypoints.Add(new Vector2(0, 0));
        Waypoints.Add(new Vector2(1, 0));
        Waypoints.Add(new Vector2(2, 0));
    }
}
