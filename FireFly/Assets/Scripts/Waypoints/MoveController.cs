﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveController : MonoBehaviour {
    private Transform cachedTransform;
    public float Duration = 5;
    private float progress = 0;
    private WaypointList waypoints;

    void Awake()
    {
        cachedTransform = transform;
    }

    void Start () {
        waypoints = WaypointManager.Instance.Routes[0];
        cachedTransform.position = waypoints.Waypoints[0];
	}

    // Update is called once per frame
    void Update()
    {
        progress += Time.deltaTime / Duration;
        if (progress < 1f)
        {
            Debug.Log(waypoints.GetPoint(progress));
            cachedTransform.position = waypoints.GetPoint(progress);
        }
    }
}
