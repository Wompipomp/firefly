﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WaypointManager : MonoBehaviour {
    public static WaypointManager Instance;
    public WaypointList[] Routes { get; private set; }
    public WaypointManager()
    {
        Instance = this;
    }

    void Awake() {
        Routes = gameObject.GetComponentsInChildren<WaypointList>();
    }

    public WaypointList GetRandomWaypointRoute()
    {
        return Routes[Random.Range(0, Routes.Length)];
    }

    public int Count()
    {
        return Routes.Length;
    }
	
	
}
