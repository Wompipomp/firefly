﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Screentyper : MonoBehaviour {

    [Multiline]
    public string Text;
    public Text ScreenText;
    public float Delay = 0.1f;
    public float DelayForLineBreaks = 1f;
	// Use this for initialization
	void Start () {
        
        StartCoroutine(WriteToScreen());
	}
	
    public IEnumerator WriteToScreen()
    {
        yield return new WaitForSeconds(1);
        for(int i = 0; i < Text.Length; i++)
        {
            ScreenText.text = Text.Substring(0, i);
            if (Text[i] == '\n')
                yield return new WaitForSeconds(DelayForLineBreaks);
            else
                yield return new WaitForSeconds(Delay);
        }
       
    }

    void Update()
    {
        if(Input.anyKeyDown)
        {
            SceneManager.Instance.GoNextLevel();
        }
    }
}
