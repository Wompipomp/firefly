﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class MainMenuController : MonoBehaviour {

    private Settings settings;
    public GameObject OptionsMenuView;
    private OptionsMenu optionsMenuScript;
    public Button ExitButton;

    void Awake()
    {
        optionsMenuScript = OptionsMenuView.GetComponent<OptionsMenu>();
        settings = new Settings();
    }

    public void Start()
    {
        gameObject.SetActive(true);
        OptionsMenuView.SetActive(false);
        SoundController.Instance.SetMusicVol(settings.GetMusicVolume());
        SoundController.Instance.StartPlayingMainMenuMusic();
#if UNITY_WEBPLAYER || UNITY_WEBGL
        Debug.Log("test");
        ExitButton.gameObject.SetActive(false);
#endif
    }

    public void StartGame()
    {
        SoundController.Instance.StopPlayingBackgroundMusic();
        SceneManager.Instance.GoNextLevel();
    }

    public void Options()
    {
        optionsMenuScript.Initialize(gameObject);
        gameObject.SetActive(false);
        OptionsMenuView.SetActive(true);
         
    }

    public void Exit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif

    }




}
