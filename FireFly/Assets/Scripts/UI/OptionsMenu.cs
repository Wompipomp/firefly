﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionsMenu : MonoBehaviour {
    private Settings settings;
    public Slider MusicVolSlider;
    public Text MusicVolText;
    public Slider FxVolSlider;
    public Text FxVolText;
    private GameObject callingObject;

    public OptionsMenu()
    {
        settings = new Settings();
    }

    public void Initialize(GameObject caller)
    {
        var musicVol = settings.GetMusicVolume() * 100;
        MusicVolSlider.value = musicVol;
        MusicVolText.text = musicVol.ToString();
        var fxVol = settings.GetFxVolume() * 100;
        FxVolSlider.value = fxVol;
        FxVolText.text = fxVol.ToString();
        callingObject = caller;
        
    }

    public void ReturnToCaller()
    {
        callingObject.SetActive(true);
        gameObject.SetActive(false);
    }

    public void OnValueChangedMusicVolSlider(float value)
    {
        MusicVolText.text = value.ToString();
        settings.SetMusicVolume(value / 100);
        SoundController.Instance.SetMusicVol(settings.GetMusicVolume());
    }

    public void OnValueChangedFxVolSlider(float value)
    {
        FxVolText.text = value.ToString();
        settings.SetFxVolume(value / 100);
        SoundController.Instance.SetFxVol(settings.GetFxVolume());
    }
}
