﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class UIController : MonoBehaviour {
    public static UIController Instance;

    public Text Lifes;
    public Text Points;
    public Text SceneText;
    public Slider Healthbar;
    public Text Combo;
    public Text Credits;
    public Canvas ParentCanvas;

    public UIController()
    {
        Instance = this;
    }

    public void StartGetReadyUiAnimation()
    {
        StartCoroutine(StartAnimation());
    }

    private IEnumerator StartAnimation()
    {
        for(int i = 3; i > 0; i--)
        {
            SetSceneText(i.ToString());
            yield return new WaitForSeconds(1);
        }
        SetSceneText("GO!");
        yield return new WaitForSeconds(1);
        SceneText.enabled = false;
    }

    public void SetLifes(int lifes)
    {
        if(Lifes != null)
            Lifes.text = "Lifes: " + lifes;
    }

    public void SetHealth(int health)
    {
        health = Mathf.Clamp(health,0, 100);
        Healthbar.value = health;
    }

    public void SetPoints(int points)
    {
        if(Points != null)
            Points.text = "Points: " + points;
    }

    public void SetSceneText(string text)
    {
        SetSceneTextVisible(true);
        SceneText.text = text;
    }

    private void SetSceneTextVisible(bool visible)
    {
        SceneText.enabled = visible;
    }


    public void SetCombo(int multiplier)
    {
        Combo.text = "Combo: x" + multiplier;
    }

    public IEnumerator ShowCredits(float waitForSeconds)
    {
        SetSceneText("Game over");
        yield return new WaitForSeconds(waitForSeconds);
        var credits = (Text)Instantiate(Credits, Vector3.zero, Quaternion.identity);
        credits.transform.SetParent(ParentCanvas.transform, false);
    }
}
