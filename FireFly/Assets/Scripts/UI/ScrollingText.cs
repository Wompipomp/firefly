﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScrollingText : MonoBehaviour {
    public bool EnableScrolling;
    public float ScrollingSpeed;
    public Vector3 Direction;

    public Text TextToScroll;

	void Update () {
	    if(EnableScrolling)
        {
            var toScroll = ScrollingSpeed * Time.deltaTime;
            TextToScroll.transform.Translate(Direction * toScroll);
        }
	}
}
