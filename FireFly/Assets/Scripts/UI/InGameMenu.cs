﻿using UnityEngine;
using System.Collections;

public class InGameMenu : MonoBehaviour {
    public GameObject OptionsMenu;
    private OptionsMenu optionsMenuScript;
    
    public void Awake()
    {
        optionsMenuScript = OptionsMenu.GetComponent<OptionsMenu>();
    }
     
    public void ExitToMainMenu()
    {
       GameManager.Instance.ReturnToMainScreen();
    }

    public void Options()
    {
        gameObject.SetActive(false);
        optionsMenuScript.Initialize(gameObject);
        OptionsMenu.SetActive(true);
    }

    public void ReturnToGame()
    {
        
        GameManager.Instance.ReturnToGame();
    }

  
}
