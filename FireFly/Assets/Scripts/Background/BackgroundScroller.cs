﻿using UnityEngine;
using System.Collections;

public class BackgroundScroller : MonoBehaviour {

    public GameObject[] Backgrounds;
    public float ForegroundSpeed;

    private ParallaxBackgroundObject[] cachedObjects;
    private Vector3 cameraPosition;
    private float allSpriteWidth;

    private bool active = true;
    

    void Start()
    {
        cachedObjects = new ParallaxBackgroundObject[Backgrounds.Length];
        for (int i = 0; i < Backgrounds.Length; i++)
        {
            var parallaxObject = new ParallaxBackgroundObject();
            parallaxObject.Transform = Backgrounds[i].transform;
            parallaxObject.Sprite = Backgrounds[i].GetComponent<SpriteRenderer>();
            parallaxObject.Renderer = Backgrounds[i].GetComponent<Renderer>();
            cachedObjects[i] = parallaxObject;
            allSpriteWidth += parallaxObject.Sprite.bounds.size.x;
        }
       
        cameraPosition = Camera.main.transform.position;
    }

    void OnEnable()
    {
        MessageSystem.Instance.AddListener(MessageSystem.StartPauseEvent, SetInactive);
        MessageSystem.Instance.AddListener(MessageSystem.EndPauseEvent, SetActive);
    }

    public void SetInactive()
    {
        active = false;
    }

    public void SetActive()
    {
        active = true;
    }

	// Update is called once per frame
	void Update () {
        if (!active)
            return;
        for (int i = 0; i < cachedObjects.Length; i++)
        {
            var position = cachedObjects[i].Transform.position;
            cachedObjects[i].Transform.position = new Vector3(position.x - ForegroundSpeed, position.y, position.z);
            if (cameraPosition.x > cachedObjects[i].Transform.position.x)
            {
                if (!cachedObjects[i].Renderer.IsVisibleFrom(Camera.main))
                {
                    cachedObjects[i].Transform.position = new Vector3(cachedObjects[i].Transform.position.x + allSpriteWidth, position.y, position.z);
                }
            }
        }
            
            
        
	}
}
