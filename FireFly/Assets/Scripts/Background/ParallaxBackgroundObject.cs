﻿using UnityEngine;
using System.Collections;

public class ParallaxBackgroundObject {
    public Renderer Renderer { get; set; }
    public Transform Transform { get; set; }
    public SpriteRenderer Sprite { get; set; }
	
}
