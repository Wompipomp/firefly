﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponController : MonoBehaviour {
    public GameObject[] Projectiles;
    public GameObject[] Missiles;

    public Transform RocketSlotPosition1;
    public Transform RocketSlotPosition2;
    public float RocketReloadTime;

    public Transform WeaponSlotPosition;

    private List<BaseProjectile> projectileScripts;
    
    private bool canFireProjectile;
    private bool canFireRocket;
    private bool fireFromAbove;

    private int upgrades = 0;


    private int currentProjectile;
    private bool isThreeWeaponSystemInUse;

    private int currentMissile;



    void Start()
    {
        canFireProjectile = true;
        canFireRocket = true;

        projectileScripts = new List<BaseProjectile>();
        
        for(int i = 0; i < Projectiles.Length; i++)
        {
            projectileScripts.Add(Projectiles[i].GetComponent<BaseProjectile>());
        }

        currentProjectile = 0;
        currentMissile = -1;

        
    }

    private void SetSingleWeaponSystem()
    {
        isThreeWeaponSystemInUse = false;
    }

    private void SetThreeWeaponSystem()
    {
        isThreeWeaponSystemInUse = true;
        
    }

    public void FireProjectile()
    {
        if(canFireProjectile){
            if(isThreeWeaponSystemInUse)
            {
                FireThreeShots();
            }
            else
            {
                FireSingleShot();
            }
            
            canFireProjectile = false;
            Invoke("Reload", projectileScripts[currentProjectile].ReloadTime);  
        }
    }

    public void FireRocket()
    {
            if (canFireRocket && currentMissile >= 0)
            {
                canFireRocket = false;
            SoundController.Instance.PlayRocketSound();
                SpawnController.Instance.Spawn(Missiles[currentMissile], fireFromAbove ? RocketSlotPosition1.position : RocketSlotPosition2.position, fireFromAbove ? Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 10))) : Quaternion.Euler(new Vector3(0, 0, Random.Range(0, -10))));
            fireFromAbove = !fireFromAbove;
                Invoke("ReloadRocket", RocketReloadTime);
            }
    }

  
    public void UpgradeWeapon()
    {
        upgrades++;
        if(!isThreeWeaponSystemInUse)
        {
            SetThreeWeaponSystem();
        }
        else
        {
            if (!IsMaxUpgraded())
            {
                currentProjectile++;
                SetSingleWeaponSystem();
            }

        }
        if((upgrades % 3) == 0)
        {
            currentMissile++;
            if (currentMissile >= Missiles.Length)
                currentMissile = Missiles.Length -1;
            
        }

    }

    private bool IsMaxUpgraded()
    {
        return currentProjectile == Projectiles.Length - 1;
    }

    

    public void Reload()
    {
        canFireProjectile = true;
    }

    public void ReloadRocket()
    {
        canFireRocket = true;
    }

   
    private void FireSingleShot()
    {
        SoundController.Instance.PlaySingleShot();
        SpawnController.Instance.Spawn(Projectiles[currentProjectile], WeaponSlotPosition.position, WeaponSlotPosition.rotation);
    }

    private void FireThreeShots()
    {
        SoundController.Instance.PlayMultiShot();
        SpawnController.Instance.Spawn(Projectiles[currentProjectile], WeaponSlotPosition.position, Quaternion.identity);
        SpawnController.Instance.Spawn(Projectiles[currentProjectile], WeaponSlotPosition.position, Quaternion.Euler(new Vector3(0, 0, 20)));
        SpawnController.Instance.Spawn(Projectiles[currentProjectile], WeaponSlotPosition.position, Quaternion.Euler(new Vector3(0, 0, -10)));

    }


}
