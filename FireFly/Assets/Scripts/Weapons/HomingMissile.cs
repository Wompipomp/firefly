﻿using UnityEngine;
using System.Collections;

public class HomingMissile : BaseRocket
{
    public bool GuidedMissile = false;
	public float RotationSpeed;
	public Transform Target;

	private Rigidbody2D rb;
	
	private bool lockedTarget = false;
    

	// Use this for initialization
	public override void Start () {
		var enemy = GameObject.FindGameObjectWithTag("Enemy");
		if(enemy != null)
		{ 
			Target = enemy.GetComponent<Transform>();
			lockedTarget = true;
		}
        	
		enableMovement = true;
        base.Start();
	}
	
	// Update is called once per frame
	public override void Update () {
		if (enableMovement)
		{
            if (currentSpeed > Random.Range(4,6) && GuidedMissile)
            {
                //Rotate
                Vector3 targetVector;
                if (lockedTarget && Target != null)
                    targetVector = Target.position - cachedTransform.position;
                else
                    targetVector = new Vector3(100, 0, 0);
                var targetAngle = Mathf.Atan2(targetVector.y, targetVector.x) * Mathf.Rad2Deg;
                var q = Quaternion.AngleAxis(targetAngle, Vector3.forward);
                cachedTransform.rotation = Quaternion.Slerp(cachedTransform.rotation, q, Time.deltaTime * currentSpeed);

            }
			MoveRocket();
		}
	}



}
