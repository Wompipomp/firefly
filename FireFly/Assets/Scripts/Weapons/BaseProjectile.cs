﻿using UnityEngine;
using System.Collections;

public class BaseProjectile : MonoBehaviour {

	public float ShotSpeed = 10;
    public float ReloadTime = 2;
    public int Damage = 10;
    public bool DestroyOnImpact = true;
    public LayerMask HitMask;

    Transform cachedTransform;
	// Use this for initialization
	void Start () {
		cachedTransform = GetComponent<Transform>();
        Destroy(gameObject, 2);
    }
	
	void Update()
	{
		cachedTransform.Translate(ShotSpeed * Time.deltaTime, 0, 0);
	}

    void OnTriggerEnter2D(Collider2D other)
    {
            if (!HitMask.MatchLayer(other.gameObject.layer)) //Kein Treffer
                  return;
            SoundController.Instance.PlayHitSound();
            SpawnController.Instance.SpawnExplosion(ExplosionsIds.ExplosionSmall, cachedTransform.position);
            other.gameObject.SendMessage("AddDamage", Damage, SendMessageOptions.DontRequireReceiver);
            if (DestroyOnImpact)
            {
                Destroy(gameObject);
            }
    }

	 
}
