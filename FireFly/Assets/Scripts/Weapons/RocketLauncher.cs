﻿using UnityEngine;
using System.Collections;

public class RocketLauncher : MonoBehaviour {

    public Transform _rocketSlot;
    public GameObject UnguidedMissile;
    public GameObject GuidedMissile;
       
    public void LaunchSingleGuidedMissile()
    {
        SpawnController.Instance.Spawn(GuidedMissile, _rocketSlot.position, _rocketSlot.rotation * Quaternion.Euler(0, 0, Random.Range(-10, 25)));
    }

    public void LaunchBulkGuidedMissile()
    {
        StartCoroutine(FireBulkRockets());
    }

    public void LaunchMissile()
    {
        SpawnController.Instance.Spawn(UnguidedMissile, _rocketSlot.position, _rocketSlot.rotation * Quaternion.Euler(0, 0, Random.Range(-15, 0)));
    }

    private IEnumerator FireBulkRockets()
    {
        for (int i = 0; i < 5; i++)
        {
            yield return new WaitForEndOfFrame();
            SpawnController.Instance.Spawn(GuidedMissile, _rocketSlot.position, _rocketSlot.rotation * Quaternion.Euler(0, 0, Random.Range(0, 25)));
        }
    }
}
