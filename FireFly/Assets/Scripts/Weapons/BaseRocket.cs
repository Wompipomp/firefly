﻿using UnityEngine;
using System.Collections;

public class BaseRocket : MonoBehaviour
{
    public float Acceleration;
    public float MaxSpeed;
    public float ReloadTime = 2;
    public float ReloadTimeBulk = 2;
    public int Damage = 5;
    public LayerMask HitMask;

    private SpriteRenderer sprite;

    protected bool enableMovement;
    protected Transform cachedTransform;

    protected float currentSpeed;

    void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();

    }

    public virtual void Start()
    {
        cachedTransform = transform;
      
    }
    
    public virtual void Update()
    {
        MoveRocket();
    }

    protected void MoveRocket()
    {
        //Translate
        currentSpeed += Acceleration;
        currentSpeed = Mathf.Min(currentSpeed, MaxSpeed);
        cachedTransform.Translate(Vector3.right * currentSpeed * Time.deltaTime);

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (!HitMask.MatchLayer(other.gameObject.layer))
            return;
        sprite.enabled = false;
        if (other.gameObject.CompareTag("Enemy"))
        {
            other.gameObject.GetComponent<BaseEnemyController>().AddDamage(Damage);
        }
        SpawnController.Instance.SpawnExplosion(ExplosionsIds.Explosion, cachedTransform.position);
        Destroy(gameObject, 2);
        enableMovement = false;
    }


    public void SetStartSpeed(float speed)
    {
        currentSpeed = Mathf.Clamp(speed,0,100);
    }
}
