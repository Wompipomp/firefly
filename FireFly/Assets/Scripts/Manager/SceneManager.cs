﻿using UnityEngine;
using System.Collections;

public class SceneManager : PersistentSingleton<SceneManager> {
    public const int MainMenu = 0;
    public const int LoadingScene = 1;
    public const int Level = 2;
  
        public void LoadLevel(int level)
    {
        StopAllCoroutines();
       if(level < 0 || level >= Application.levelCount)
        {
            Debug.Log("Error loaded Level out of Bounds");
            return;
        }
        Application.LoadLevel(level);
    }

    public void GoNextLevel()
    {
        StopAllCoroutines();
        if (Application.loadedLevel < Application.levelCount - 1)
        {
            Debug.Log("Error loaded Level out of Bounds");
        }
        Application.LoadLevel(Application.loadedLevel + 1);

    }

    public int LoadedLevel
    {
        get
        {
            return Application.loadedLevel;
        }
    }
}
