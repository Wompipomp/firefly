﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class SoundController : PersistentSingleton<SoundController> {
    public AudioClip[] BackgroundMusic;
    public AudioClip MainMenuMusic;
    public AudioClip[] Other;
    public AudioClip[] Explosions;
    public AudioClip[] Shots;
    public AudioClip[] HitSounds;
    public AudioClip RocketClip;
    public bool PlayRandomMusic;
    public AudioMixer Master;

    public AudioSource BackgroundMusicSource;
    public AudioSource DefaultFxSource;
    public AudioSource ExplosionsSource;
    public AudioSource ShotSource;

    private const string bgMusicVolume = "bgMusicVolume";
    private const string fxVolume = "fxVolume";

    private int bgMusicIdx;

    public void StartPlayingMainMenuMusic()
    {
        
        BackgroundMusicSource.clip = MainMenuMusic;
        BackgroundMusicSource.loop = true;
        BackgroundMusicSource.Play();
    }

    public void StopPlayingBackgroundMusic()
    {
        BackgroundMusicSource.Stop();
    }

    void StartPlayingInGameMusic()
    {
        bgMusicIdx = 0;
        if (PlayRandomMusic)
            bgMusicIdx = Random.Range(0, BackgroundMusic.Length);
        BackgroundMusicSource.clip = BackgroundMusic[bgMusicIdx];
        BackgroundMusicSource.loop = false;
        BackgroundMusicSource.Play();
    }

    void Update()
    {
        if (SceneManager.Instance.LoadedLevel == SceneManager.MainMenu)
            return;
        if (!BackgroundMusicSource.isPlaying)
        {
            if (PlayRandomMusic)
            {
                var lastIdx = bgMusicIdx;
                do
                {
                    bgMusicIdx = Random.Range(0, BackgroundMusic.Length); //Never play same song twice
                } while (lastIdx == bgMusicIdx);

            }
            else
                bgMusicIdx++;

            BackgroundMusicSource.clip = BackgroundMusic[bgMusicIdx];
            BackgroundMusicSource.Play();
        }
    }

  
    public void PlayRandomEnemyExplosion()
    {
        var clip = Explosions[Random.Range(0, Explosions.Length)];
        ExplosionsSource.PlayOneShot(clip);
    }

    public void PlaySingleShot()
    {
        ShotSource.PlayOneShot(Shots[0]);
    }

    public void PlayMultiShot()
    {
        ShotSource.PlayOneShot(Shots[1]);
    }

    public void SetMusicVol(float volume)
    {
        Master.SetFloat(bgMusicVolume, volume.VolumeToAttenuation());
    }

    public void SetFxVol(float volume)
    {
        Master.SetFloat(fxVolume, volume.VolumeToAttenuation());
    }

    public void PlayHitSound()
    {
        var clip = HitSounds[Random.Range(0, HitSounds.Length)];
        ShotSource.PlayOneShot(clip);
    }

    public void PlayRocketSound()
    {
        DefaultFxSource.PlayOneShot(RocketClip);
    }

}


