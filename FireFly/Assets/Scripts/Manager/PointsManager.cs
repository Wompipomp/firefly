﻿using UnityEngine;
using System.Collections;

public class PointsManager : MonoBehaviour {

    public float TimeSpanForMultiplierAdd;
    public float TimeSpanForMultiplierReset = 1;


    private float timeOfLastPoints;
    public int Multiplier { get; set; }
    public int MaxMultiplier = 10;

    public UserManager UserManager { get; set; }

    void Start()
    {
        Multiplier = 1;
        UIController.Instance.SetCombo(Multiplier);
    }

    public void AddPoints(int points)
    {
        var timeSpan = Time.time - timeOfLastPoints;
        if (timeSpan < TimeSpanForMultiplierAdd)
        {
            Multiplier++;
            Multiplier = Mathf.Min(MaxMultiplier, Multiplier);

        }
        timeOfLastPoints = Time.time;
        UserManager.AddPoints(points * Multiplier);
        Debug.Log("Points: " + points * Multiplier);
        UIController.Instance.SetPoints(UserManager.GetPoints());
        UIController.Instance.SetCombo(Multiplier);
    }

    void Update()
    {
        if (Time.time - timeOfLastPoints > TimeSpanForMultiplierReset)
        {
            Multiplier = 1;
            UIController.Instance.SetCombo(Multiplier);
        }

    }


}
