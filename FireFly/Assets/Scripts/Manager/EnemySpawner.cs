﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class EnemySpawnItem
{
    public GameObject[] EnemyPrefabList;
    public float SpawnDelay;
    public float WaveDelay;
    public float Speed;
}

public class EnemySpawner : MonoBehaviour {
    public EnemySpawnItem[] EnemyList;
    private float halfVerticalSize;
    private float halfHorizontalSize;

    void Start()
    {
        GetVisibleBoundaries();
        StartCoroutine(SpawnEnemies());
    }

    private void GetVisibleBoundaries()
    {
        halfVerticalSize = Camera.main.orthographicSize - 1;
        halfHorizontalSize = Camera.main.orthographicHorizontalSize();
    }

    private IEnumerator SpawnEnemies()
    {
        GameObject enemy = null;
        for (int i = 0; i < EnemyList.Length; i++)
        {
            yield return new WaitForSeconds(EnemyList[i].WaveDelay);
            var currentEnemy = EnemyList[i];
            var waypointRoute = WaypointManager.Instance.GetRandomWaypointRoute();

            for (int enemies = 0; enemies < currentEnemy.EnemyPrefabList.Length; enemies++)
            {
                enemy = SpawnController.Instance.Spawn(currentEnemy.EnemyPrefabList[enemies], Vector3.zero, Quaternion.identity) as GameObject;
                var initDto = new InitEnemyDto(currentEnemy.Speed, waypointRoute);
                var enemyController = enemy.GetComponent<BaseEnemyController>();
                enemy.transform.position = SetPosition(waypointRoute, enemyController);
                enemy.GetComponent<BaseEnemyController>().Init(initDto);
                yield return new WaitForSeconds(currentEnemy.SpawnDelay);
            }
        }

        //Gameover:
        while(enemy != null) //Wait till last enemy is killed or last boss
        {
            yield return null;
        }
        yield return new WaitForSeconds(4);
        StartCoroutine(GameManager.Instance.GameFinished());
    }

    private Vector3 SetPosition(WaypointList waypointRoute, BaseEnemyController enemyController)
    {
        Vector3 position;
        if (enemyController is EnemyWaypointController)
            position = waypointRoute.Waypoints[0];
        else
        {
            position = new Vector3(halfHorizontalSize + 1, Random.Range(-halfVerticalSize, halfVerticalSize), 0);
        }

        return position;
    }



}

public class InitEnemyDto
{
    public InitEnemyDto(float speed, WaypointList waypoints)
    {
        Speed = speed;
        Waypoints = waypoints;
    }
    public float Speed { get; set; }
    public WaypointList Waypoints { get; set; }
}

