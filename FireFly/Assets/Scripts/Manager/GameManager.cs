﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public enum GameState
{
    Playing,
    Finished
}

public class GameManager : MonoBehaviour
{
    private GameState currentState = GameState.Playing;
    public static GameManager Instance { get; private set; }

    public GameObject PlayerPrefab;
    private Vector3 beginGameStartPosition;
    private Vector3 beginGameEndPosition;
    public float StartTime = 3;
    public float StartTimeInvulnerable = 4;
    public AnimationCurve StartAnimation;
    public AnimationCurve EndAnimation;
    public float RespawnWaitTime;
    public GameObject Player { get; set; }
    public GameObject InGameMenu;
    private UserManager userManager;
    private PlayerController playerController;
    private PointsManager pointsManager;
    private float defaultTimeScale = 1f;
    private bool guiVisible = false;
    private readonly Settings settings;
    public float EndTime;


    public GameManager()
    {
        Instance = this;
        settings = new Settings();
    }
    
    public void Awake()
    {
        pointsManager = GetComponent<PointsManager>();
    }

    public void Start()
    {
        Init();
    }

    private void Init()
    {
        CreateNewPlayer();
        userManager.Init();
        
        UIController.Instance.SetLifes(userManager.GetLifes());
        UIController.Instance.SetHealth(userManager.GetHealth());
        UIController.Instance.SetPoints(userManager.GetPoints());
        
        SoundController.Instance.SetMusicVol(settings.GetMusicVolume());
        SoundController.Instance.SetFxVol(settings.GetFxVolume());
        StartPlayerAnimation();
        InvokeStartGame();
    }

    private void InvokeStartGame()
    {
        Invoke("StartGame", StartTime);
    }

    private void CreateNewPlayer()
    {
        Player = SpawnController.Instance.Spawn(PlayerPrefab, beginGameStartPosition, Quaternion.identity);
        Player.SetActive(false);
        playerController = Player.GetComponent<PlayerController>();
        userManager = playerController.UserManager;
        pointsManager.UserManager = userManager;
        
    }

    public IEnumerator GameFinished()
    {
        SoundController.Instance.StartPlayingMainMenuMusic();
        playerController.SetMoveable(false);
        Debug.Log(Player.transform.position);
        Debug.Log(Player.transform.position + new Vector3(15,0,0));
        StartCoroutine(MoveToPosition(EndTime, Player.transform.position, Player.transform.position + new Vector3(15,0,0), EndAnimation));
        StartCoroutine(UIController.Instance.ShowCredits(EndTime + 1f));
        yield return new WaitForSeconds(4);
        currentState = GameState.Finished;
        yield return new WaitForSeconds(16);
        SceneManager.Instance.LoadLevel(SceneManager.MainMenu);
    }

    private void StartPlayerAnimation()
    {
        Player.SetActive(true);
        CalculateStartAnimation();
        StartCoroutine(MoveToPosition(StartTime, beginGameStartPosition, beginGameEndPosition, StartAnimation));
        StartCoroutine(playerController.Invulnerable(StartTimeInvulnerable));
        UIController.Instance.StartGetReadyUiAnimation();
        
    }

    private void CalculateStartAnimation()
    {
        var horizontalSize = -Camera.main.orthographicHorizontalSize();
        beginGameStartPosition = new Vector3(horizontalSize - 1, 0, 0);
        beginGameEndPosition = new Vector3(horizontalSize + 3, 0, 0);
    }

   

    public void AddPoints(int points)
    {
        pointsManager.AddPoints(points);
    }

    private void StartGame()
    {
        playerController.SetMoveable(true);
    }

  
    private IEnumerator MoveToPosition(float waitTime, Vector3 startPosition, Vector3 endPosition, AnimationCurve curve)
    {
        var elapsedTime = 0f;
        while (elapsedTime < waitTime)
        {
            Player.transform.position = Vector3.Lerp(startPosition, endPosition, curve.Evaluate(elapsedTime));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }


    public void KillPlayer()
    {
        userManager.LostLife();
        UIController.Instance.SetLifes(userManager.GetLifes());
        if (userManager.HasLife())
        {
            Invoke("RespawnPlayer", RespawnWaitTime);
        }
        else
        {
            Invoke("GameOver", RespawnWaitTime);
        }
    }

    private void GameOver()
    { 
        currentState = GameState.Finished;
        
        UIController.Instance.SetSceneText("Game over");
    }

    

    private void RespawnPlayer()
    {
        StartPlayerAnimation();
        userManager.SetHealth(100);
        UIController.Instance.SetHealth(userManager.GetHealth());
        InvokeStartGame();
    }

    private void Update()
    {
        if (currentState == GameState.Playing)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!guiVisible)
                    ShowIngameMenu();
                else if (guiVisible && InGameMenu.activeSelf)
                    ReturnToGame();
            }
        }
        if (currentState == GameState.Finished)
        {
            if (Input.anyKeyDown)
            {
                SceneManager.Instance.LoadLevel(SceneManager.MainMenu);
            }
        }

    }

    private void ShowIngameMenu()
    {
        SetTimeScale(0);
        guiVisible = true;
        MessageSystem.Instance.Invoke(MessageSystem.StartPauseEvent);
        InGameMenu.SetActive(true);
    }

    private void SetTimeScale(float timeScale)
    {
        defaultTimeScale = Time.timeScale;
        Time.timeScale = timeScale;
    }

    public void ReturnToGame()
    {
        InGameMenu.SetActive(false);
        ResetLastTimeScale();
        MessageSystem.Instance.Invoke(MessageSystem.EndPauseEvent);
        guiVisible = false;
    }

    private void ResetLastTimeScale()
    {
        Time.timeScale = defaultTimeScale;
    }

    public void ReturnToMainScreen()
    {
        ResetLastTimeScale();
        SceneManager.Instance.LoadLevel(SceneManager.MainMenu);
    }

}
