﻿using UnityEngine;
using System.Collections;

public class UpgradeSpawner : MonoBehaviour {

    public int Delay;
    public GameObject Prefab;
    private float halfVerticalSize;
    private float halfHorizontalSize;
	// Use this for initialization
	void Start () {
        GetVisibleBoundaries();
        StartCoroutine(CreateUpgrades());
	}
	
	private IEnumerator CreateUpgrades()
    {
         
        for(int i = 0; i < 20; i++)
        {
            yield return new WaitForSeconds(Delay);
            SpawnController.Instance.Spawn(Prefab, new Vector3(halfHorizontalSize + 1, Random.Range(halfVerticalSize, -halfVerticalSize), 0), Quaternion.identity);

        }
    }

    private void GetVisibleBoundaries()
    {
        halfVerticalSize = Camera.main.orthographicSize - 1;
        halfHorizontalSize = Camera.main.orthographicHorizontalSize();
    }
}
