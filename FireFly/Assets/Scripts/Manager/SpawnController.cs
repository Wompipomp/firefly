﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnController : MonoBehaviour {

    private static SpawnController instance;
    public List<GameObject> ExplosionPrefabs;

    public SpawnController()
    {
        if(instance != null)
        {
            Debug.Log("Instantiate more than one SpawnController");
            return;
        }
        instance = this;
    }

    public static SpawnController Instance
    {
        get
        {
            return instance;
        }
    }

    public GameObject Spawn(GameObject anObject, Vector3 position, Quaternion rotation)
    {
        return Instantiate(anObject, position, rotation) as GameObject;
    }

    public GameObject SpawnExplosion(ExplosionsIds id, Vector3 position, bool randomDirection = true)
    {
        return Instantiate(ExplosionPrefabs[(int)id], position, randomDirection ? Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 359))) : Quaternion.identity) as GameObject;
    }


}

public enum ExplosionsIds
{
    ExplosionSmall = 0,
    Explosion,
    ExplosionBig,
    PlayerExplosion
}
