﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerController : MonoBehaviour {

    public float MovingSpeed;
    public float BoundaryOffset = 0.6f;
    public float BoundaryOffsetBottom = 2f;
    public UserManager UserManager { get; private set; }

    //PlayerComponents
    private WeaponController weaponController;    
    private InputController input;
    private SpriteRenderer spriteRenderer;
    private Transform cachedTransform;
    private Animator anim;
    private Camera mainCam;
    private float maxXPlayerRight;
    private bool canMove;
    private float currentHorizontalSpeed;
    private bool isInvulnerable;
    void Awake()
    {
        cachedTransform = transform;
        anim = GetComponent<Animator>();
        input = GetComponent<InputController>();
        weaponController = GetComponent<WeaponController>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        UserManager = new UserManager(new UserDataManager());
        mainCam = Camera.main;
    }

   

    void Start()
    {
        maxXPlayerRight = DetermineMaxPlayerXPosition();
        SetMoveable(false);
    }

    private float DetermineMaxPlayerXPosition()
    {
        var maxCameraX = Camera.main.orthographicHorizontalSize();
        return maxCameraX * 0.5f;
    }


    public void SetMoveable(bool moveable)
    {
        canMove = moveable;
    }

    public IEnumerator Invulnerable(float timeSpan)
    {
        isInvulnerable = true;
        var lastFrameTime = Time.time;
        var elapsedTime = 0f;        
        while(elapsedTime < timeSpan)
        {
            spriteRenderer.enabled = !spriteRenderer.enabled;
            elapsedTime += Time.time - lastFrameTime;
            lastFrameTime = Time.time;
            yield return new WaitForSeconds(0.2f);
        }
        spriteRenderer.enabled = true;
        isInvulnerable = false;
    }
    
    		
	// Update is called once per frame
	void Update () {
        if (!canMove)
            return;
        var horizontal = input.Horizontal;
        var vertical = input.Vertical;
        var fire = input.Fire1;
        
        currentHorizontalSpeed = horizontal * MovingSpeed;

        ClampToBoundaries();
        cachedTransform.Translate(currentHorizontalSpeed * Time.deltaTime, vertical * MovingSpeed * Time.deltaTime, 0);

        if (fire)
        {
            weaponController.FireProjectile();
            weaponController.FireRocket();
        }
        anim.SetFloat("Steering", vertical);
	}

    void LateUpdate()
    {
        input.CheckInput();
    }

    private void ClampToBoundaries()
    {
        var currentPosition = transform.position;
        var verticalSize = mainCam.orthographicSize;
        var horizontalSize = mainCam.orthographicHorizontalSize();

        var clampedX = Mathf.Clamp(currentPosition.x, -horizontalSize + BoundaryOffset, maxXPlayerRight);
        var clampedY = Mathf.Clamp(currentPosition.y, -verticalSize + BoundaryOffsetBottom, verticalSize - BoundaryOffset);
        transform.position = new Vector3(clampedX, clampedY, currentPosition.z);
 
          
    }

    public void UpgradeWeapon()
    {
        weaponController.UpgradeWeapon();
    }

    public void AddDamage(int damage)
    {
        if (!isInvulnerable)
        {
            UserManager.AddDamage(damage);
        }
        if (UserManager.GetHealth() <= 0)
        {
            UserManager.SetHealth(0);
            KillPlayer();
            GameManager.Instance.KillPlayer();
        }
        UIController.Instance.SetHealth(UserManager.GetHealth());
    }

    private void KillPlayer()
    {
        canMove = false;
        SpawnController.Instance.SpawnExplosion(ExplosionsIds.PlayerExplosion, gameObject.transform.position, false);
        gameObject.SetActive(false);
    }
}
