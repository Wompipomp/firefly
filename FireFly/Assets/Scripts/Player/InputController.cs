﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour
{
    public bool ActivateInput = true;
    public float Horizontal { get; set; }
    public float Vertical { get; set; }
    public bool Fire1 { get; set; }
    public bool Fire2 { get; set; }

    public void CheckInput()
    {
        if (!ActivateInput)
            return;
        Horizontal = Input.GetAxis("Horizontal");
        Vertical = Input.GetAxis("Vertical");
        Fire1 = Input.GetKey(KeyCode.K);
        //Fire2 = Input.GetKey(KeyCode.J);
    }

}