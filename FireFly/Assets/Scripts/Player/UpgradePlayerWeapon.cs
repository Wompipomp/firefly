﻿using UnityEngine;
using System.Collections;

public class UpgradePlayerWeapon : MonoBehaviour {
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag(Tags.Player))
        {
            var playerController = other.GetComponent<PlayerController>();
            playerController.UpgradeWeapon();
            Destroy(gameObject);
        }
        
    }
}
