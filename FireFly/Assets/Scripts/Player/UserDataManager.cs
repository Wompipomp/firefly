﻿using UnityEngine;
using System.Collections;

public class UserDataManager {

    public UserDataManager()
    {
        Health = 100;
        Points = 0;
    }
    public int Health { get; set; }
    public int Points { get; set; }

    public int Lifes { get; set; }


}
