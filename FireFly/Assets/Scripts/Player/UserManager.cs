﻿using UnityEngine;
using System.Collections;
using System;

public class UserManager {

    UserDataManager _userData;
    public UserManager(UserDataManager userData)
    {
        _userData = userData;
    }

    public void Init()
    {
        _userData.Health = 100;
        _userData.Lifes = 3;
        _userData.Points = 0;
    }

    public void AddPoints(int points)
    {
        _userData.Points += points;

    }

    public void AddDamage(int damage)
    {
        _userData.Health -= damage;
    }

    public int GetHealth()
    {
        return _userData.Health;
    }

    internal void LostLife()
    {
        _userData.Lifes -= 1;
    }

    internal bool HasLife()
    {
        return _userData.Lifes > 0;
    }

    public int GetLifes()
    {
        return _userData.Lifes;
    }

    public void SetHealth(int health)
    {
        _userData.Health = health;
    }

    public int GetPoints()
    {
        return _userData.Points;
    }
}
