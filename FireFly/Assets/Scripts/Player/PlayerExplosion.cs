﻿using UnityEngine;
using System.Collections;

public class PlayerExplosion : MonoBehaviour {

    public GameObject ExplosionBig;
    public GameObject ExplosionMedium;
    public GameObject ExplosionSmall;

    public int ExplosionRounds = 6;
    public int SmallExplosionsPerRound = 4;
    public float DelayPerRound = 0.3f;
    public Vector2 MinOffset = new Vector2(-1, -1);
    public Vector2 MaxOffset = new Vector2(1, 1);

	// Use this for initialization
	void Start () {
        StartCoroutine(ExplosionStart());    
	}
	
	
    public IEnumerator ExplosionStart()
    {
        SpawnController.Instance.SpawnExplosion(ExplosionsIds.ExplosionBig, transform.position, false);
        SoundController.Instance.PlayRandomEnemyExplosion();
        yield return new WaitForSeconds(0.1f);
        for (int i = 0; i < ExplosionRounds; i++)
        {
            SpawnController.Instance.SpawnExplosion(ExplosionsIds.Explosion, transform.position + new Vector3(Random.Range(MinOffset.x, MaxOffset.x), Random.Range(MinOffset.y, MaxOffset.y), 0));
            SoundController.Instance.PlayRandomEnemyExplosion();
            for (int j = 0; j < SmallExplosionsPerRound; j++)
            {
                SoundController.Instance.PlayRandomEnemyExplosion();
                SpawnController.Instance.SpawnExplosion(ExplosionsIds.ExplosionSmall, transform.position + new Vector3(Random.Range(MinOffset.x, MaxOffset.x), Random.Range(MinOffset.y, MaxOffset.y), 0));
            }
            yield return new WaitForSeconds(DelayPerRound);
        }

    }
}
