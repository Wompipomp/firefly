﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(WaypointList))]
public class WaypointListInspector : Editor {

	void OnSceneGUI()
    {
        var controller = target as WaypointList;
        if (controller.Waypoints != null && controller.Waypoints.Count < 3)
            return;

        var lineStart = controller.GetPoint(controller.Waypoints[0], controller.Waypoints[1], controller.Waypoints[2], 0f);
        for (int i = 0; i < controller.Waypoints.Count - 2; i+=2)
        {
            Handles.color = Color.grey;
            var p0 = controller.Waypoints[i];
            var p1 = controller.Waypoints[i + 1];
            Handles.DrawLine(p0, p1);
            Handles.DrawLine(p1, controller.Waypoints[i + 2]);
         

            for (int j = 0; j <= controller.LineSteps; j++)
            {
                var lineEnd = controller.GetPoint(controller.Waypoints[i], controller.Waypoints[i + 1], controller.Waypoints[i + 2], j / (float)controller.LineSteps);
           
                Handles.color = Color.white;

                Handles.DrawLine(lineStart, lineEnd);
                lineStart = lineEnd;
            }
        }

        for (int i = 0; i < controller.Waypoints.Count; i++)
        {
            var p0 = controller.Waypoints[i];
            EditorGUI.BeginChangeCheck();
            p0 = Handles.DoPositionHandle(p0, Quaternion.identity);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "Move point");
                EditorUtility.SetDirty(target);
                controller.Waypoints[i] = p0;
            }
        }
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if(GUILayout.Button("Reset"))
        {
            var controller = target as WaypointList;
            controller.Reset();
            EditorUtility.SetDirty(target);
        }
    }

  
}
