FireFly

2D side scrolling shmup with Unity3d 5.0

Attributions:
Graphics:
Pixel Explosion Art Pack (https://www.patreon.com/creation?hid=1908952) by Ansimuz
Dust Mountain Background (https://www.patreon.com/creation?hid=1812199) by Ansimuz
Space Ship Shooter (https://www.patreon.com/creation?hid=1645657) by Ansimuz
Gradius Clone Sprites: http://opengameart.org/content/gradius-clone-sprites by surt

Music:
Artblock: http://opengameart.org/content/artblock by Jan125
Burgers: http://opengameart.org/content/burgers by Jan125
CausticChip: http://opengameart.org/content/caustic-chip by Jan125
Jum to win: http://opengameart.org/content/neocrey-jump-to-win by neocrey
Plan: http://opengameart.org/content/plan by Sudocolon
Raining Bits: http://opengameart.org/content/raining-bits by Gundatsch - https://soundcloud.com/gundatsch

FX:
Retro Sound Effects: https://www.assetstore.unity3d.com/en/#!/content/22153 by mulula

Font:
Press Start 2P: http://openfontlibrary.org/en/font/press-start-2p